﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphAPI
{
    /// <summary>
    /// This class provides the creation of HTML like labels.
    /// Thanks to this we can form html text with different styles.
    /// The LabelHtml class is an additional function for the graph.
    /// </summary>
    public class LabelHtml
    {
        /// <summary>
        /// Sets the tag font
        /// </summary>
        /// <param name="htmlString">
        /// String for framing in the tag
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetFont(string htmlString)
        {
            htmlString = string.Concat("<font>",
                htmlString,
                "</font>");

            return htmlString;
        }

        /// <summary>
        /// Sets the bold font
        /// </summary>
        /// <param name="htmlString">
        /// String for framing in the tag
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetBold(string htmlString)
        {
            htmlString = string.Concat("<b>",
                htmlString,
                "</b>");

            return htmlString;
        }

        /// <summary>
        /// Sets the italic font
        /// </summary>
        /// <param name="htmlString">
        /// String for framing in the tag
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetItalics(string htmlString)
        {
            htmlString = string.Concat("<i>",
                htmlString,
                "</i>");

            return htmlString;
        }

        /// <summary>
        /// Sets the line break
        /// </summary>
        /// <param name="htmlString">
        /// The tape at the end of which set CARRIAGE RETURN and LINE FEED
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetLineFeed()
        {
            string htmlString = "<br/>";

            return htmlString;
        }

        /// <summary>
        /// Sets the font color
        /// </summary>
        /// <param name="htmlString">
        /// String of which set color font
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetColor(string htmlString, string color)
        {
            string htmlCss = string.Concat("<font ", "color", "=", "'", color, "'", ">");

            htmlString = htmlString.Replace("<font>", htmlCss);

            return htmlString;
        }

        /// <summary>
        /// Sets the font color
        /// </summary>
        /// <param name="htmlString">
        /// String of which set size font
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string SetPointSize(string htmlString, int pointSize)
        {
            string htmlCss = string.Concat("<font ", "point-size", "=", "'", pointSize, "'", ">");

            htmlString = htmlString.Replace("<font>", htmlCss);

            return htmlString;
        }

        /// <summary>
        /// Return block of code as a string, that description of label
        /// </summary>
        /// <param name="labelString">
        /// one or more strings which are combined into a single tape
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public string GetString(params string[] labelString)
        {
            string resultString = string.Empty;
            foreach (var item in labelString)
            {
                resultString += item + " ";
            }

            resultString = resultString.Replace(" ,", ",");
            resultString = string.Concat("<", resultString.Trim(), ">");

            return resultString;
        }
    }
}
