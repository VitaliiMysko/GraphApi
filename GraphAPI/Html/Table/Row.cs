﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphAPI
{
    /// <summary>
    /// This class generates row HTML code when creating a table.
    /// The Row class is an additional function for the graph.
    /// </summary>
    public class Row
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public List<Cell> Cell { get; set; } = new List<Cell>();
        public Parameters Parameters { get; set; } = new Parameters();

        /// <summary>
        /// Initializes a new instance of the Row class
        /// </summary>
        /// <param name="id">
        /// The identifier of the row
        public Row(string id)
        {
            this.Id = id;
        }

        /// <summary>
        /// Adds text for row
        /// </summary>
        /// <param name="text">
        /// Text in row
        /// </param>
        public void AddText(string text)
        {
            this.Text = text;
        }
    }
}
