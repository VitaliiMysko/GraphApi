﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphAPI
{
    /// <summary>
    /// This class generates table HTML code.
    /// The TableHtml class is an additional function for the graph.
    /// </summary>
    public class TableHtml
    {
        public string Id { get; set; }
        public List<Row> Rows { get; set; } = new List<Row>();
        public Parameters Parameters { get; set; } = new Parameters();

        /// <summary>
        /// Initializes a new instance of the TableHtml class
        /// </summary>
        /// <param name="id">
        /// The identifier of the table
        public TableHtml(string Id)
        {
            this.Id = Id;   
        }

        /// <summary>
        /// Creates a new table graph
        /// </summary>
        /// <param name="Rows">
        /// one or more rows which will be included in the table
        /// </param>
        public void CreateTable(params Row[] Rows)
        {
            foreach (var item in Rows)
            {
                this.Rows.Add(item);
            }
        }

        /// <summary>
        /// Return block of code as a string, that create of table
        /// </summary>
        /// <returns>
        /// String html
        /// </returns>
        public string GetString()
        {
            string tableString = string.Empty;
            tableString += string.Concat("<");
            tableString += GetTableString(Parameters, Rows);
            tableString += string.Concat("\r\n", ">");


            return tableString;
        }

        private string GetTableString(Parameters parameters, List<Row> rows)
        {
            string tableString = string.Empty;
            tableString += string.Concat("<table", " ");

            foreach (var parametr in parameters)
            {
                tableString += string.Concat(parametr.Key, "=", parametr.Value, " ");
            }

            tableString = tableString.Trim();

            tableString += string.Concat(">", "\r\n");

            foreach (var row in rows)
            {
                tableString += "<tr";

                foreach (var parametr in row.Parameters)
                {
                    tableString += string.Concat(parametr.Key, "=", parametr.Value, " ");
                }
                tableString = tableString.Trim();

                tableString += string.Concat(">");

                foreach (var cell in row.Cell)
                {
                    tableString += string.Concat("<td", " ");

                    foreach (var parametr in cell.Parameters)
                    {
                        tableString += string.Concat(parametr.Key, "=", parametr.Value, " ");
                    }
                    tableString = tableString.Trim();

                    tableString += string.Concat(">");
                    if (cell.Table != null)
                    {
                        string secondaryTable = GetTableString(cell.Table.Parameters, cell.Table.Rows);
                        tableString += secondaryTable;
                    }

                    tableString += cell.Text;
                    tableString += string.Concat("</td>");
                }

                int positionLastTr = tableString.LastIndexOf("<tr>");
                string subString = tableString.Substring(positionLastTr - 4);

                //--if block <tr></tr> is empty, it removed
                if (subString.IndexOf("td") == -1)
                {
                    tableString = tableString.Substring(0, positionLastTr);
                    continue;
                }
                else
                    tableString += string.Concat("</tr>", "\r\n");
            }

            tableString = string.Concat("\r\n", tableString, "</table>");

            return tableString;
        }
    }
}
