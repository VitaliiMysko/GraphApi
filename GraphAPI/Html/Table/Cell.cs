﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphAPI
{
    /// <summary>
    /// This class generates cell HTML code when creating a table.
    /// The Cell class is an additional function for the graph.
    /// </summary>
    public class Cell
    {
        public string Id { get; set; }
        public string Text { get; set; } = string.Empty;
        public Parameters Parameters { get; set; } = new Parameters();
        public TableHtml Table { get; set; }

        /// <summary>
        /// Initializes a new instance of the Cell class
        /// </summary>
        /// <param name="id">
        /// The identifier of the cell
        public Cell(string id)
        {
            this.Id = id;
        }

        /// <summary>
        /// Adds text for cell
        /// </summary>
        /// <param name="text">
        /// Text in cell
        /// </param>
        public void AddText(string text)
        {
            this.Text = text;
        }
    }
}
