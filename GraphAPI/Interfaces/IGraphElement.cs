﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphAPI.Interfaces
{
    /// <summary>
    /// Interface for all elements of the graph including the graph itself
    /// </summary>
    interface IGraphElement
    {
        string Id { get; set; }
        Parameters Parameters { get; set; }
        /// <summary>
        /// Returns the string according to the implementation
        /// </summary>
        string GetString();
    }
}
