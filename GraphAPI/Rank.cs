﻿using GraphAPI.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GraphAPI
{
    /// <summary>
    /// This class provides a grouping of nodes for a better and simpler-looking graph.
    /// By this one, we can manage the structure of the graph explicitly instead of the default logic. 
    /// Class Rank is an additional feature for graph.
    /// </summary>
    public class Rank : IGraphElement
    {
        public string Id { get; set; }
        public Parameters Parameters { get; set; } = new Parameters();

        public List<Node> GroupNodes { get; set; } = new List<Node>();

        /// <summary>
        /// Initializes a new instance of the Rank class
        /// </summary>
        /// <param name="id">
        /// The identifier of the rank
        /// </param>
        public Rank(string id)
        {
            Id = id;
        }

        /// <summary>
        /// Initializes a new instance of the Rank class 
        /// </summary>
        /// <param name="id">
        /// The identifier of the rank
        /// </param>
        /// <param name="parameters">
        /// Parameters for the rank
        /// </param>
        public Rank(string id, Parameters parameters)
        {
            Id = id;
            Parameters = parameters;
        }

        /// <summary>
        /// Add list of nodes to group of rank
        /// </summary>
        /// <param name="nodes">
        /// List of nodes that need to group
        /// </param>
        public void AddGroupNodes(List<Node> nodes)
        {
            GroupNodes.AddRange(nodes);
        }

        /// <summary>
        /// Remove nodes from group
        /// </summary>
        /// <param name="nodes">
        /// one or more node that need to find and delete from the group
        /// </param>
        public void RemoveNodes(params Node[] nodes)
        {
            foreach (var node in nodes)
            {
                GroupNodes = GroupNodes.Where(r => r.Id != node.Id).ToList();
            }
        }

        /// <summary>
        /// Return block of code as a string, that description of rank
        /// </summary>
        /// <returns>
        /// String
        /// </returns>
        public string GetString()
        {
            if (GroupNodes.Count == 0)
                return "";

            var result = "{";

            if (Parameters.Count == 0)
            {
                //by default
                Parameters.Add("rank", "same");
            }

            foreach (var parameter in Parameters)
            {
                result += $"{parameter.Key} = {parameter.Value}";

                if (parameter.Key != Parameters.Keys.ToList().Last()) result += ','; //if it's not last element add ','
            }

            result += "; " + string.Join(" ", GroupNodes.Select(i => i.Id));

            result += "}";

            //{ rank=same A B C D E }
            return result;
        }
    }
}
