﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphAPI
{
    public class Graph : IGraphElement
    {
        public string Id { get; set; }

        public List<Rank> Ranks { get; set; } = new List<Rank>();
        public List<Graph> SubGraphs { get; private set; } = new List<Graph>();
        public List<Node> Nodes { get; set; } = new List<Node>();
        public List<Connection> Connections { get; set; } = new List<Connection>();
        public string ConnectionsType { get; private set; } = "--";
        public Parameters Parameters { get; set; } = new Parameters();
        public Node DefaultNode = new Node("node");

        public Graph(string id)
        {
            this.Id = id;
        }

        public Graph(string id, Parameters parameters)
        {
            Id = id;
            Parameters = parameters;
        }

        /// <summary>
        /// Sets connection type in every connection, subgraph and field connectionsType
        /// </summary>
        public void SetConectionsType(string connectionsType)
        {
            this.ConnectionsType = connectionsType;
            foreach (var conection in Connections)
            {
                conection.connectionType = connectionsType;
            }
            foreach (var subgraph in SubGraphs)
            {
                subgraph.SetConectionsType(connectionsType);
            }
        }
        /// <summary>
        /// Add nodes to field Nodes
        /// </summary>
        public void AddNodes(params Node[] nodes)
        {
            Nodes.AddRange(nodes);
        }

        /// <summary>
        /// Remove nodes from field Nodes
        /// </summary>
        public void RemoveNodes(params Node[] nodes)
        {
            foreach (var node in nodes)
            {
                Nodes.Remove(node);
            }
        }

        /// <summary>
        /// Add connection to field Connections. All nodes connect to first node
        /// </summary>
        public void AddConnections(Node node, params Node[] nodes)
        {
            foreach (var _node in nodes)
            {
                Connections.Add(new Connection(node, _node, ConnectionsType));
            }
        }

        /// <summary>
        /// Add connection to field Connections. All nodes connect to first node
        /// </summary>
        public void AddConnections(Parameters parametrs, Node nodeA, params Node[] nodes)
        {
            foreach (var node in nodes)
            {
                Connections.Add(new Connection(nodeA, node, ConnectionsType, parametrs));
            }
        }

        /// <summary>
        /// Removes all connections that contain these two nodes.
        /// </summary>
        public void RemoveConection(Node nodeStart, Node nodeEnd)
        {
            Connections.RemoveAll(i => i.StartNode == nodeStart && i.EndNode == nodeEnd);
        }

        /// Removes all connections that contain node which not present in field Nodes
        /// </summary>
        public void RemoveInactiveConections()
        {
            var nodes = GetAllNodes();

            var removeConnections = new List<Connection>();
            foreach (var conection in Connections)
            {
                if (!nodes.Any(i => conection.StartNode == i))
                {
                    removeConnections.Add(conection);
                    continue;
                }
                if (!nodes.Any(i => conection.EndNode == i))
                {
                    removeConnections.Add(conection);
                }
            }

            foreach (var conection in removeConnections)
            {
                Connections.Remove(conection);
            }
        }
        /// <summary>
        /// Get all nodes from graph and subgraphs
        /// </summary>
        public List<Node> GetAllNodes()
        {
            var nodes = new List<Node>();

            nodes.AddRange(Nodes.Where(i => true).ToList());

            foreach (var graph in SubGraphs)
            {
                nodes.AddRange(graph.Nodes.Where(i => !nodes.Any(j => j.Id == i.Id))); //add all nodes that not present in List "nodes"
            }

            return nodes;
        }

        /// <summary>
        /// Add graph into field SubGraphs
        /// </summary>
        public void AddSubgraph(Graph subGraph)
        {
            bool isExists = SubGraphs.Any(x => x.Id == subGraph.Id);
            if (isExists)
                throw new ArgumentException("Graph already contains a subgraph with such an identifier. Cannot be added!");

            SubGraphs.Add(subGraph);
        }

        /// <summary>
        /// Remove graph from field SubGraphs
        /// </summary>
        public void RemoveSubgraph(Graph subGraph)
        {
            if (subGraph != null)
            {
                bool isExists = SubGraphs.Any(x => x.Id == subGraph.Id);
                if (!isExists)
                    throw new ArgumentException("SubGraph does not exist!");

                SubGraphs.Remove(subGraph);
            }
        }

        /// <summary>
        /// Creates and adds a rank based on the passed parameters
        /// </summary>
        public void AddRank(string id, params Node[] nodes)
        {
            var rank = new Rank(id);
            rank.AddGroupNodes(nodes.ToList());
            Ranks.Add(rank);
        }

        /// <summary>
        /// Creates and adds a rank based on the passed parameters
        /// </summary>
        public void AddRank(string id, Parameters parametrs, params Node[] nodes)
        {
            var rank = new Rank(id, parametrs);
            rank.AddGroupNodes(nodes.ToList());
            Ranks.Add(rank);
        }

        /// <summary>
        /// Return string with next content:
        ///[Type of graph] [graph ID]{
        ///[Parameters]
        ///[Default node parameters]
        ///[Nodes]
        ///[Conections]
        ///[Subgraph]
        ///[Rank]}
        /// </summary>
        public string GetString()
        {
            string result = "graph";

            if (ConnectionsType == "->")
            {
                result = "digraph";
            }

            if (!string.IsNullOrEmpty(Id))
            {
                result += " " + Id;
            }

            result += "{\n";

            foreach (var parameter in Parameters)
            {
                result += $"{parameter.Key} = {parameter.Value};\n";
            }

            if (DefaultNode.Parameters.Count > 0)
            {
                result += DefaultNode.GetString() + '\n';
            }

            foreach (var node in Nodes)
            {
                result += node.GetString() + '\n';
            }

            foreach (var conection in Connections)
            {
                result += conection.GetString() + '\n';
            }

            foreach (var subgraph in SubGraphs)
            {
                var subgraphStr = subgraph.GetString();

                subgraphStr = "subgraph cluster_" + subgraphStr.Substring(subgraphStr.IndexOf(" ") + 1);

                result += subgraphStr + '\n';
            }


            foreach (var rank in Ranks)
            {
                result += rank.GetString() + '\n';
            }

            result += '}';
            return result;
        }

    }
}