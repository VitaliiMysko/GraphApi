﻿using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphAPI
{
    public class Connection : IGraphElement
    {
        public Parameters Parameters { get; set; } = new Parameters();
        public string Id { get; set; }
        public string connectionType { get; set; }
        public Node StartNode;
        public Node EndNode;

        public Connection(Node startNode, Node endNode, string conectionType)
        {
            StartNode = startNode;
            EndNode = endNode;
            this.connectionType = conectionType;
        }

        public Connection(Node startNode, Node endNode, string conectionType, Parameters parameters)
        {
            StartNode = startNode;
            EndNode = endNode;
            this.connectionType = conectionType;
            this.Parameters = parameters;
        }
        /// <summary>
        /// Return string with next content:
        ///{StartNode.Id} {connectionType}{ EndNode.Id}[{Parameters.Key} = { Parameters.Value}];
        /// </summary>
        /// <returns></returns>
        public string GetString()
        {
            var result = $"{StartNode.Id} {connectionType} {EndNode.Id}";

            if (Parameters.Count != 0) result += '[';

            foreach (var parameter in Parameters)
            {
                result += $"{parameter.Key} = {parameter.Value}";

                if (parameter.Key != Parameters.Keys.ToList().Last()) result += ','; //if it's not last element add ','
            }

            if (Parameters.Count != 0) result += ']';

            result += ";";

            return result;
        }
    }
}
