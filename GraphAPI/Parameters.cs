﻿using System.Collections.Generic;

namespace GraphAPI
{
    /// <summary>
    /// inherit a dictionary<string, string>
    /// </summary>
    public class Parameters : Dictionary<string, string>
    {
        public new string this[string key]
        {
            get
            {
                if (base.ContainsKey(key))
                    return base[key];
                else
                    return string.Empty;
            }
            set
            {
                base[key] = value;
            }
        }
    }
}