﻿using GraphAPI.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GraphAPI
{
    public class Node : IGraphElement
    {
        public string Id { get; set; }
        public Parameters Parameters { get; set; } = new Parameters();


        public Node(string id)
        {
            Id = id;
        }

        public Node(string id, Parameters parameters)
        {
            Id = id;
            Parameters = parameters;
        }

        /// <summary>
        /// Return string with next content:
        ///{Id}[{Parameters.Key} = { Parameters.Value}];
        /// </summary>
        public string GetString()
        {
            var result = Id;

            if (Parameters.Count != 0) result += '[';

            foreach (var parameter in Parameters)
            {
                result += $"{parameter.Key} = {parameter.Value}";

                if (parameter.Key != Parameters.Keys.ToList().Last()) result += ','; //if it's not last element add ','
            }

            if (Parameters.Count != 0) result += ']';

            result += ";";

            return result;
        }
    }
}