﻿using NUnit.Framework;

namespace GraphAPI.Test
{
    public class GraphTest
    {
        [Test]
        public void GetString_GetResultTheSimpleGraph()
        {
            //Example 1: Simple Graph

            //given
            var graph = new Graph("");
            graph.SetConectionsType("--");

            var nodeA = new Node("a");
            var nodeB = new Node("b");
            var nodeC = new Node("c");
            var nodeD = new Node("d");
            var nodeE = new Node("e");

            graph.AddConnections(nodeA, nodeB);
            graph.AddConnections(nodeB, nodeC);
            graph.AddConnections(nodeA, nodeC);
            graph.AddConnections(nodeD, nodeC);
            graph.AddConnections(nodeE, nodeC);
            graph.AddConnections(nodeE, nodeA);

            var expectedResult = string.Concat(
                "graph{\n",
                    "a -- b;\n",
                    "b -- c;\n",
                    "a -- c;\n",
                    "d -- c;\n",
                    "e -- c;\n",
                    "e -- a;\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetString_GetResultTheSimpleDigraph()
        {
            //Example 3: Simple Digraph

            //given
            var graph = new Graph("");
            graph.SetConectionsType("->");

            Parameters paramDefaultNode = new Parameters();
            paramDefaultNode.Add("shape", "plaintext");

            graph.DefaultNode.Parameters = paramDefaultNode;

            var nodeA = new Node("a");
            var nodeB = new Node("b");
            var nodeC = new Node("c");
            var nodeD = new Node("d");

            graph.AddConnections(nodeA, nodeB);
            graph.AddConnections(nodeB, nodeC);
            graph.AddConnections(nodeC, nodeD);
            graph.AddConnections(nodeD, nodeA);

            var expectedResult = string.Concat(
                "digraph{\n",
                    "node[shape = plaintext];\n",
                    "a -> b;\n",
                    "b -> c;\n",
                    "c -> d;\n",
                    "d -> a;\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetString_GetResultTheFullSimpleDigraphWithDifferentConnections()
        {
            //Mix
            //Example 4: Full Digraph
            //Example 5: Showing A Path

            //given
            var graph = new Graph("");
            graph.SetConectionsType("->");

            Parameters connectParamAB = new Parameters();
            connectParamAB.Add("label", "0.2");
            connectParamAB.Add("weight", "0.2");

            Parameters connectParamAC = new Parameters();
            connectParamAC.Add("label", "0.4");
            connectParamAC.Add("weight", "0.4");

            Parameters connectParamCE = new Parameters();
            connectParamCE.Add("label", "0.6");
            connectParamCE.Add("weight", "0.6");

            Parameters connectParamEE = new Parameters();
            connectParamEE.Add("label", "0.1");
            connectParamEE.Add("weight", "0.1");

            Parameters connectParamEB = new Parameters();
            connectParamEB.Add("label", "0.7");
            connectParamEB.Add("weight", "0.7");

            Parameters connectParamCB = new Parameters();
            connectParamCB.Add("label", "0.6");
            connectParamCB.Add("weight", "0.6");


            Parameters connectParamWABC = new Parameters();
            connectParamWABC.Add("color", "red");
            connectParamWABC.Add("penwidth", "3.0");


            var nodeA = new Node("a");
            var nodeB = new Node("b");
            var nodeC = new Node("c");
            var nodeE = new Node("e");
            var nodeW = new Node("w");

            graph.AddConnections(connectParamAB, nodeA, nodeB);
            graph.AddConnections(connectParamAC, nodeA, nodeC);
            graph.AddConnections(nodeC, nodeB);
            graph.AddConnections(connectParamCE, nodeC, nodeE);
            graph.AddConnections(connectParamEE, nodeE, nodeE);
            graph.AddConnections(connectParamEB, nodeE, nodeB);

            graph.AddConnections(connectParamWABC, nodeW, nodeA, nodeB, nodeC);

            var expectedResult = string.Concat(
               "digraph{\n",
                    "a -> b[label = 0.2,weight = 0.2];\n",
                    "a -> c[label = 0.4,weight = 0.4];\n",
                    "c -> b;\n",
                    "c -> e[label = 0.6,weight = 0.6];\n",
                    "e -> e[label = 0.1,weight = 0.1];\n",
                    "e -> b[label = 0.7,weight = 0.7];\n",
                    "w -> a[color = red,penwidth = 3.0];\n",
                    "w -> b[color = red,penwidth = 3.0];\n",
                    "w -> c[color = red,penwidth = 3.0];\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetString_GetResultTheDigraphWithSubgraphs()
        {
            //Example 6: Subgraphs

            //given
            var graph = new Graph("");
            //graph.SetConectionsType("->");

            var nodeA = new Node("a");
            var nodeB = new Node("b");
            var nodeC = new Node("c");
            var nodeD = new Node("d");
            var nodeF = new Node("f");


            Parameters paramCluster0 = new Parameters();
            paramCluster0.Add("label", "\"Subgraph A\"");

            var subGraphCluster0 = new Graph("0", paramCluster0);
            //subGraphCluster0.SetConectionsType("->");

            subGraphCluster0.AddConnections(nodeA, nodeB);
            subGraphCluster0.AddConnections(nodeB, nodeC);
            subGraphCluster0.AddConnections(nodeC, nodeD);

            graph.AddSubgraph(subGraphCluster0);


            Parameters paramCluster1 = new Parameters();
            paramCluster1.Add("label", "\"Subgraph B\"");

            var subGraphCluster1 = new Graph("1", paramCluster1);
            //subGraphCluster1.SetConectionsType("->");

            subGraphCluster1.AddConnections(nodeA, nodeF);
            subGraphCluster1.AddConnections(nodeF, nodeC);

            graph.AddSubgraph(subGraphCluster1);

            graph.SetConectionsType("->");

            var expectedResult = string.Concat(
               "digraph{\n",
                    "subgraph cluster_0{\n",
                        "label = \"Subgraph A\";\n",
                        "a -> b;\n",
                        "b -> c;\n",
                        "c -> d;\n",
                    "}\n",
                        "subgraph cluster_1{\n",
                        "label = \"Subgraph B\";\n",
                        "a -> f;\n",
                        "f -> c;\n",
                    "}\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetString_GetResultLargeGraphsWithRank()
        {
            //Example 7: Large Graphs

            //given
            Parameters paramGraph = new Parameters();
            paramGraph.Add("rankdir", "LR");

            var graph = new Graph("", paramGraph);
            graph.SetConectionsType("--");

            Parameters paramA = new Parameters();
            paramA.Add("label", "\"A\"");

            Parameters paramC = new Parameters();
            paramC.Add("label", "\"C\"");

            Parameters paramD = new Parameters();
            paramD.Add("label", "\"D\"");

            var nodeA = new Node("a", paramA);
            var nodeB = new Node("b");
            var nodeC = new Node("c", paramC);
            var nodeD = new Node("d", paramD);
            var nodeE = new Node("e");
            var nodeF = new Node("f");
            var nodeG = new Node("g");
            var nodeH = new Node("h");

            graph.AddNodes(nodeA, nodeC, nodeD);


            Parameters paramBEH = new Parameters();
            paramBEH.Add("color", "grey");
            paramBEH.Add("shape", "diamond");


            graph.AddConnections(nodeA, nodeC);
            graph.AddConnections(nodeA, nodeD);
            graph.AddConnections(paramBEH, nodeB, nodeE);
            graph.AddConnections(nodeC, nodeF);
            graph.AddConnections(nodeD, nodeF);
            graph.AddConnections(nodeD, nodeG);
            graph.AddConnections(paramBEH, nodeE, nodeH);


            Parameters paramRank = new Parameters();
            paramRank.Add("rank", "same");

            graph.AddRank("Rank1", paramRank, nodeB, nodeC, nodeD);
            graph.AddRank("Rank2", paramRank, nodeE, nodeF, nodeG);

            var expectedResult = string.Concat(
                "graph{\n",
                    "rankdir = LR;\n",
                    "a[label = \"A\"];\n",
                    "c[label = \"C\"];\n",
                    "d[label = \"D\"];\n",
                    "a -- c;\n",
                    "a -- d;\n",
                    "b -- e[color = grey,shape = diamond];\n",
                    "c -- f;\n",
                    "d -- f;\n",
                    "d -- g;\n",
                    "e -- h[color = grey,shape = diamond];\n",
                    "{rank = same; b c d}\n",
                    "{rank = same; e f g}\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetString_GetResultNestedClusters()
        {
            //Nested clusters

            //given
            var graph = new Graph("D");
            graph.SetConectionsType("->");

            var nodeA = new Node("a");
            var nodeB = new Node("b");
            var nodeC = new Node("c");
            var nodeD = new Node("d");
            var nodeE = new Node("e");

            //Parent
            Parameters paramClusterP = new Parameters();
            paramClusterP.Add("label", "\"Parent\"");

            var subGraphClusterP = new Graph("p", paramClusterP);

            graph.AddSubgraph(subGraphClusterP);

            //Child one
            Parameters paramClusterC1 = new Parameters();
            paramClusterC1.Add("label", "\"Child one\"");

            var subGraphClusterC1 = new Graph("c1", paramClusterC1);
            subGraphClusterC1.AddNodes(nodeA);

            subGraphClusterP.AddSubgraph(subGraphClusterC1);

            //Grand-Child one
            Parameters paramClusterGC1 = new Parameters();
            paramClusterGC1.Add("label", "\"Grand-Child one\"");

            var subGraphClusterGC1 = new Graph("gc_1", paramClusterGC1);
            subGraphClusterGC1.AddNodes(nodeB);

            subGraphClusterC1.AddSubgraph(subGraphClusterGC1);

            //Grand-Child two
            Parameters paramClusterGC2 = new Parameters();
            paramClusterGC2.Add("label", "\"Grand-Child two\"");

            var subGraphClusterGC2 = new Graph("gc_2", paramClusterGC2);
            subGraphClusterGC2.AddNodes(nodeC);
            subGraphClusterGC2.AddNodes(nodeD);

            subGraphClusterC1.AddSubgraph(subGraphClusterGC2);

            //Child two
            Parameters paramClusterC2 = new Parameters();
            paramClusterC2.Add("label", "\"Child two\"");

            var subGraphClusterC2 = new Graph("c2", paramClusterC2);
            subGraphClusterC2.AddNodes(nodeE);

            subGraphClusterP.AddSubgraph(subGraphClusterC2);


            var expectedResult = string.Concat(
               "digraph D{\n",
                    "subgraph cluster_p{\n",
                        "label = \"Parent\";\n",

                        "subgraph cluster_c1{\n",
                            "label = \"Child one\";\n",
                            "a;\n",

                            "subgraph cluster_gc_1{\n",
                                "label = \"Grand-Child one\";\n",
                                "b;\n",
                            "}\n",

                            "subgraph cluster_gc_2{\n",
                                "label = \"Grand-Child two\";\n",
                                "c;\n",
                                "d;\n",
                            "}\n",

                        "}\n",

                        "subgraph cluster_c2{\n",
                            "label = \"Child two\";\n",
                            "e;\n",
                        "}\n",
                    "}\n",
                "}");

            //when
            var result = graph.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }
    }
}

