﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GraphAPI.Test
{
    /// <summary>
    /// This class provides the generated tape to save to a text file for visualization in Graphviz,
    /// using syntactics DOT Language
    /// Command Line: $ dot -Tsvg input.txt > output.svg
    /// </summary>
    public class SaveStringToTxtFile
    {
        /// <summary>
        /// Saves the string to a text file
        /// </summary>
        /// <param name="outputFileName">
        /// The resulting file
        /// </param>
        /// <param name="htmlString">
        /// String to save to file
        /// </param>
        /// <returns>
        /// Bool: true or false
        /// </returns>
        public bool StringToFile(string outputFileName, string htmlString)
        {
            string pathTofileAudit = @Path.Combine("D:", "FutureProcessing", "Graphviz", outputFileName);

            if (File.Exists(pathTofileAudit))
            {
                using (StreamWriter sw = File.CreateText(pathTofileAudit))
                {
                    sw.Write(htmlString);
                }
            }

            return true;
        }
    }
}
