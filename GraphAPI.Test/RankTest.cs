﻿using NUnit.Framework;
using System.Collections.Generic;

namespace GraphAPI.Test
{
    public class RankTest
    {

        [Test]
        public void GetString_GetResultFromRankWithGroup()
        {
            //given
            List<Node> nodes = new List<Node>();

            nodes.Add(new Node("A"));
            nodes.Add(new Node("B"));
            nodes.Add(new Node("C"));
            nodes.Add(new Node("D"));
            nodes.Add(new Node("E"));

            var rank = new Rank("Rank1");
            rank.AddGroupNodes(nodes);

            Parameters paramRank = new Parameters();
            paramRank.Add("rank", "same");

            rank.Parameters = paramRank;

            var expectedResult = "{rank = same; A B C D E}";

            //when
            var result = rank.GetString();

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void RemoveNodes_DeleteSomeNodesFromGroup()
        {
            //given
            List<Node> nodes = new List<Node>();

            nodes.Add(new Node("A"));

            var nodeB = new Node("B");
            nodes.Add(nodeB);

            var nodeC = new Node("C");
            nodes.Add(nodeC);

            nodes.Add(new Node("D"));
            nodes.Add(new Node("E"));

            var nodeF = new Node("F");

            var rank = new Rank("Rank1");
            rank.AddGroupNodes(nodes);

            var expectedCountNodeIntoGroup = 3;

            //when
            rank.RemoveNodes(nodeB, nodeC, nodeF);

            //then
            Assert.AreEqual(expectedCountNodeIntoGroup, rank.GroupNodes.Count);
        }

    }
}

