﻿using NUnit.Framework;
using System.Collections.Generic;

namespace GraphAPI.Test
{
    public class HtmlHelperTest
    {
        [Test]
        public void HTMLLikeLabelOutputString()
        {
            var graph = new Graph("graphA");
            graph.SetConectionsType("->");

            var labelHtml = new LabelHtml();

            string expectedString = "<The <font color='red'><b>foo</b></font>, <br/> the <font point-size='20'>bar</font> and <br/> <i>baz</i>>";

            string string1 = "The";

            string string2 = "foo";
            string2 = labelHtml.SetBold(string2);
            string2 = labelHtml.SetFont(string2);
            string2 = labelHtml.SetColor(string2, "red");

            string string3 = ",";

            string string4 = labelHtml.SetLineFeed();

            string string5 = "the";

            string string6 = "bar";
            string6 = labelHtml.SetFont(string6);
            string6 = labelHtml.SetPointSize(string6, 20);

            string string7 = "and";
            string string8 = labelHtml.SetLineFeed();

            string string9 = "baz";
            string9 = labelHtml.SetItalics(string9);

            string labelString = labelHtml.GetString(string1, string2, string3, string4, string5, string6, string7, string8, string9);
            
            graph.Parameters.Add("label", labelString);
            graph.Parameters.Add("labelloc", "t");

            var nodeFoo = new Node("FOO");
            var nodeBar = new Node("BAR");
            var nodeBaz = new Node("BAZ");

            graph.AddConnections(nodeFoo, nodeBar);
            graph.AddConnections(nodeFoo, nodeBaz);

            graph.DefaultNode.Parameters.Add("shape", "plaintext");

            graph.AddNodes(nodeFoo, nodeBar, nodeBaz);

            string grathString = graph.GetString();

            SaveStringToTxtFile file = new SaveStringToTxtFile();
            string outputFileName = "labelHtml.txt";
            bool isFileRecorded =  file.StringToFile(outputFileName, grathString);

            Assert.AreEqual(expectedString, labelString);
            Assert.IsTrue(isFileRecorded);
        }

        [Test]
        public void HtmlTableOutputString()
        {
            var graph = new Graph("graphA");

            string expectedString = string.Concat("<", "\r\n",
                "<table color='green' cellspacing='0' border='1'>", "\r\n",
                "<tr><td>Hello World</td><td>Table Html</td><td></td></tr>", "\r\n",
                "<tr><td></td><td></td><td>Cell</td></tr>", "\r\n",
                "<tr><td colspan='3'></td></tr>", "\r\n",
                "</table>",
                "\r\n", ">");

            var table = new TableHtml("Table1");
            table.Parameters.Add("color", "'green'");
            table.Parameters.Add("cellspacing", "'0'");
            table.Parameters.Add("border", "'1'");


            //--Create Rows
            var Row1 = new Row("Row1");

            var Row2 = new Row("Row2");
            var Row3 = new Row("Row3");
            var Row4 = new Row("Row4");
            var Row5 = new Row("Row5");

            //--Create Cell
            //--Row1
            var Cell11 = new Cell("Cell11");
            Cell11.AddText("Hello World");

            var Cell12 = new Cell("Cell12");
            Cell12.AddText("Table Html");
            var Cell13 = new Cell("Cell13");
            

            //--Row3
            var Cell31 = new Cell("Cell31");
            var Cell32 = new Cell("Cell32");
            var Cell33 = new Cell("Cell33");
            Cell33.AddText("Cell");

            //--Row5
            var Cell51 = new Cell("Cell51");
            Cell51.Parameters.Add("colspan","'3'");

            Row1.Cell.Add(Cell11);
            Row1.Cell.Add(Cell12);
            Row1.Cell.Add(Cell13);

            Row3.Cell.Add(Cell31);
            Row3.Cell.Add(Cell32);
            Row3.Cell.Add(Cell33);

            Row5.Cell.Add(Cell51);

            table.CreateTable(Row1, Row2, Row3, Row4, Row5);

            string tableString = table.GetString();

            var tbl = new Node("tbl");

            graph.AddNodes(tbl);
            tbl.Parameters.Add("shape", "plaintext");
            tbl.Parameters.Add("label", tableString);

            string grathString = graph.GetString();

            SaveStringToTxtFile file = new SaveStringToTxtFile();
            string outputFileName = "tableHtml.txt";
            bool isFileRecorded = file.StringToFile(outputFileName, grathString);

            Assert.AreEqual(expectedString, tableString);
            Assert.IsTrue(isFileRecorded);
        }

        [Test]
        public void HtmlComplexTableOutputString()
        {
            var graph = new Graph("graphA");

            var table = new TableHtml("Table1");
            table.Parameters.Add("color", "'green'");
            table.Parameters.Add("cellspacing", "'0'");
            table.Parameters.Add("border", "'1'");

            //--Create Rows
            var Row1 = new Row("Row1");
            var Row2 = new Row("Row2");

            //--Create Cell
            //--Row1
            var Cell11 = new Cell("Cell11");
            Cell11.AddText("Html Cell One One");

            var Cell12 = new Cell("Cell12");
            Cell12.AddText("Html Cell One Two");

            var Cell13 = new Cell("Cell13");
            Cell13.AddText("Html Cell One Three");

            //--Row2
            var Cell21 = new Cell("Cell21");
            Cell21.Parameters.Add("colspan", "'3'");

            #region Secondary table
            var secondaryTable = new TableHtml("secondary Table");
            secondaryTable.Parameters.Add("color", "'red'");

            var Row2_1 = new Row("Row2_1");

            var Cell2_1_1 = new Cell("Cell2_1_1");
            Cell2_1_1.AddText("Secondary Table Html");

            Row2_1.Cell.Add(Cell2_1_1);
            secondaryTable.CreateTable(Row2_1);

            Cell21.Table = secondaryTable;
            #endregion

            Row1.Cell.Add(Cell11);
            Row1.Cell.Add(Cell12);
            Row1.Cell.Add(Cell13);

            Row2.Cell.Add(Cell21);

            table.CreateTable(Row1, Row2);

            string tableString = table.GetString();

            var tbl = new Node("tbl");

            graph.AddNodes(tbl);
            tbl.Parameters.Add("shape", "plaintext");
            tbl.Parameters.Add("label", tableString);

            string grathString = graph.GetString();

            SaveStringToTxtFile file = new SaveStringToTxtFile();
            string outputFileName = "secondaryTableHtml.txt";
            bool isFileRecorded = file.StringToFile(outputFileName, grathString);

            Assert.IsTrue(isFileRecorded);
        }



    }
}

