﻿using GraphAPI;
using GraphAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GraphConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var graph = new Graph("graphA");
            graph.SetConectionsType("->");

            Parameters paramA = new Parameters();
            paramA.Add("label","<B>cat</B>");
            var nodeA = new Node("A", paramA);
            var nodeB = new Node("B");
            var nodeC = new Node("C");
            var nodeD = new Node("D");
            var nodeE = new Node("E");
            
            var nodeF = new Node("F");
            var nodeG = new Node("G");


            graph.AddNodes(nodeA, nodeB, nodeC, nodeE, nodeF, nodeG); // A, B, C, E, F, G

            graph.AddRank("", nodeA, nodeB, nodeC);
            graph.Parameters.Add("rankdir","LR");
            graph.AddConnections(nodeA, nodeB, nodeC); // A -> {B, C}
            graph.AddConnections(nodeB, nodeC); //B -> C
            graph.AddConnections(nodeA, nodeE);

            graph.RemoveConection(nodeB, nodeC);

            graph.AddConnections(nodeA, nodeD);

            graph.RemoveNodes(nodeC);

            var graphB = new Graph("graphB");

            graphB.AddNodes(new Node("nodeG"));

            graph.AddSubgraph(graphB);
            var t = graph.GetString();
            ///* graph graphA {
            // * A[label = "<B>cat</B>"]
            // * B
            // * D
            // * E
            // * A -> B
            // * A -> D
            // * subgraph SubA{
            // *      A -> E
            // * }
            // * }
            // */

            Console.WriteLine(t);
        }
    }
}
